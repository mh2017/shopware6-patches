# Shopware 6 patches for faster deployment

Props to https://github.com/shyim/shopware-patch-collection


## Core

**export-options.patch**

Fixes the export to make properties like Size, Color available for Google Feeds

**variant-listing-updater.patch**

Fixes the error on bin/console dal:refresh:index where the subquery returns more then 1 row - thanks to [pottink](https://github.com/shopware/platform/pull/1718) 


    ERROR [console] Error thrown while running command "dal:refresh:index". Message: "An exception occurred while executing '\n UPDATE product SET display_group = MD5(\n CONCAT(\n LOWER(HEX(product.parent_id)),\n (SELECT
    SQLSTATE[21000]: Cardinality violation: 1242 Subquery returns more than 1 row

